# XML metadata format

A given simulation will require datasets from multiple sources and formats to generate time and space specific covariates for the agents.  `ABRiverTelem.jl` will automatically read and linearize these datasets for use.  To do this, users must provide a xml metadata sheet which describes the locations and parameters of the supplied datasets. 

All files described in a given xml metadatasheet comprise a dataset.  Multiple datasets can be used by a single simulation.  For example, you may construct a xml metadata sheet for each year of a multi-year simulation, then run all years simultaneously in a single instance. 

For tabular data, `rds` (serialised, single R data.frame objects) or `csv` files are accepted.  For spatial raster data, `NetCDF` files are expected.  Stream data is included as a geopackage file (`.gpkg`) where reaches (stream segments between detection arrays) are saved as single LINESTRING simple features. 

The xml can be divided into 3 broad sections:

- **Study area data files**:  Telemetry detection data and covariate data that can be applied to the entire study area.  These elements are children of the root node `<run_covariates/>`.
- **Reach Definitions**:  The child nodes of `<reaches/>` link the unique identifier labels of the detection arrays to the reach geometries.  Here, a reach is defined as the span of river between two subsequent detection arrays along a river.  A reach is identified by the detection array at its downstream end.  The attribute `reach_geometry_id ` will be used to extract the corresponding LINESTRING geometry describing that reach from the `.gpkg` file listed in the element  `<reach_geometry>`.
- **Path Definitions**:  Child nodes of `<paths/>` describe the unique up- or down-stream paths the migrating fish can traverse.  This allows the simulation to correctly deal with missed detections on receiver arrays.  Agents can move either direction along a path and skip detections on arrays in a path.

An example xml metadatasheet is shown below:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<run_covariates name="2016">
  <!-- ================ Study area data files ================ -->
  <!------------------- Non-spatial Covariates ------------------>
  # Variables independant of space and time, viewed by all agents
  <covariates_global file="global_covariates.csv">
    <variable label="sun_azimuth" column_name="azimuth"/>
  <covariates_global/>
  <!----------------------- Reach covariates -------------------->
  # Variables to be extracted from the reach-specific covariate data files
  <covariates_reaches time_column="time", tz="-08:00">
    <variable label="discharge" variable_name="discharge"/>
    <variable label="temperature" variable_name="temp"/>
    <variable label="FNU" variable_name="turbidity"/>
  </covariates_reaches>
  <!--------------- NetCDF netcdf covariate data ---------------->
  <!-- Time variables are expected as seconds since UNIX epoch -->
  <netcdf file="skyglow.netcdf"
    var_x="long" var_y="lat" var_time="time" tz="-00:00"
    proj4="+proj=latlon +data=WGS84" bin_mark="end">
    <variable label="skyglow" variable_name="skyglow"/>
  </netcdf>    
  <!------------------------- Array detections ------------------>
  <detections file="detections_2016.rda" 
    time_column="time" tz="−08:00"
    agent_id_column="id" array_id_column="array_id" />
  <!---------------- GIS data holding river reaches ------------>
  <reach_geometry  file="./stream_pieces.gpkg" id_attribute="id"/>
  
  <!-- ================= Reach definitions ================== -->
  <!----------- Stream Reaches (points of detection) ----------->
  <reaches>
    <reach array_id="234" reach_geometry_id="NA" reach_length="NA">
      <covariates file="USGS_2016_11370500.csv"/>
      <covariates file="USGS_2016_11377100.csv"/>
    </reach>
    <reach array_id="233" reach_length="4965.40" reach_geometry_id="113875979_2016_233">
      <covariates file="USGS_2016_11370500.csv"/>
      <covariates file="USGS_2016_11377100.csv"/>
    </reach>
    <receiver_array array_id="232" reach_length="641.99" reach_geometry_id="113875979_2016_232">
      <covariates file="USGS_2016_11370500.csv"/>
      <covariates file="USGS_2016_11377100.csv"/>
    </reach>
    <reach array_id="213" reach_geometry_id="NA" reach_length="NA">
      <covariates file="USGS_2016_11377100.csv"/>
      <covariates file="USGS_2016_11389500.csv"/>
    </reach>
    <reach array_id="212" reach_distance="9506.99" reach_geometry_id="113875979_2016_212">
      <covariates file="USGS_2016_11389500.csv"/>
    </reach>
    <reach array_id="211" reach_distance="32653.47" reach_geometry_id="113875979_2016_211">
      <covariates file="USGS_2016_11389500.csv"/>
    </reach>
  </reaches>

  <!-- ================== Path defintions =================== -->
  <!-------------- Unique downstream detection paths ----------->
  <paths>
    <path id="0001">
      <array id="234"/>
      <array id="233"/>
      <array id="232"/>
    </path>
    <path id="0002">
      <array id="213"/>
      <array id="212"/>
    </path>
  </paths>
</run_covariates>
```



NetCDF4

- 