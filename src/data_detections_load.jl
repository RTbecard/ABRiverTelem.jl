#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>. 
#
#    Copyright 2021 James Campbell
# =============================================================================
# Functions for loading detections data
# =============================================================================

# ========================== External Functions ===============================
"""
    readdetections(xml_file)

Read detections from xml metadata sheet.
"""
function load_detections(xml_file::AbstractString)

    n_xml = misc_loadxml(xml_file)

    # Get table linking array ids to reach ids
    arrdct = load_detections_arraydict(n_xml)
    # Get run paths
    paths = getpaths(n_xml)

    # Get metadata for detections file
    n_detections = findfirst("run_covariates/detections", n_xml)
    args = [joinpath(dirname(xml_file), n_detections["file"]),
            n_detections["agent_id_column"],
            n_detections["array_id_column"],
            n_detections["time_column"],
            arrdct, paths]
    # Read detections
    runstatvec = readdetections(args...)

    return runstatvec

end

# ========================= Internal Functions ================================


"""
    load_detections_getrunpaths(xml)

For a XML node, return a Vector{Vector{Symbol}} holding all unique run paths.
"""
function load_detections_getrunpaths(xml::EzXML.Document)

    ns_path = findall("run_covariates/paths/path", xml)

    paths = Vector{Vector{Symbol}}()

    for n_path in ns_path
        push!(paths, (x -> Symbol(x["id"])).(findall("array", n_path)))
    end

    @debug "Read Paths: ", paths

    return paths

end

"""
    load_detections_getreachtuple(arrayids, paths, arraydict)

Return a ReachTuple, which is a tuple holding all reaches the agent ran through
between the array pair listed in `arrayids`.  If there is no valid path between
the arrays, return `nothing`.
"""
function load_detections_getreachtuple(arrayids::Tuple{Symbol, Symbol},
        paths::Vector{Vector{Symbol}}, arraydict::Dict{Symbol, Symbol})

    # Loop possible paths
    for path in paths
        idx = (findfirst(isequal(arrayids[1]), path),
               findfirst(isequal(arrayids[2]), path))
    
        @debug "Path Indicies: " idx, arrayids, path

        # Check that path indicies are valid
        # (arrays at start of path have no reach_id)
        any(isnothing, idx) && continue
        idx[1] > idx[2] && continue

        # Save path as reach tuple and exit function
        @debug "arraydict: ", keys(arraydict)
        return Tuple(arraydict[path[i]] for i in (idx[1]+1):idx[2])
    end

    return nothing
end

"""
    load_detections_file(file, col_agentid, col_arrayid, col_time)

From a tabular file (csv or rds), load detections data and return a
RunStatVector suitable for run speed analysis.

This will assume all agents completed their runs, hence `reachidx` values will
be set to `-1`.  `ReachTuple`s will only include reaches the individuals were 
detected along.  For participation analysis (i.e. survival), `RunStatVector`s 
will have to be manually initalized.
"""
function load_detections_file(file::AbstractString, col_agentid::AbstractString,
        col_arrayid::AbstractString, col_time::AbstractString,
        arraydict::Dict{Symbol, Symbol}, paths::Vector{Vector{Symbol}})

    data = misc_readtabular(file)
    # ------ Check that columns exist
    for col in [col_agentid, col_arrayid, col_time]
        if col ∉ names(data)
            @error """Column "$col" missing from file "$file". \n\n$(first(data, 6))"""
            error("Invalid detections file provided.")
        end
    end

    # Convert id columns to symbols
    misc_makeidentifier!(data, col_agentid)
    misc_makeidentifier!(data, col_arrayid)

    # get unique agent ids
    agents = unique(data[!, col_agentid])

    rsvec = RunStatVector()

    # ------ Loop all unique agents
    for agent in agents
        agent_sym = Symbol(agent)
        # Subset selected agent
        datasub = subset(data, col_agentid => ByRow(==(agent)))
        # Sort by datetime
        sort!(datasub, col_time)

        # ------ Loop through detection pairs
        for row in 1:(size(datasub)[1] - 1)

            # ----- Check if agent has moved between detection arrays
            if datasub[row, col_arrayid] != datasub[row + 1, col_arrayid]

                # Get reach tuple
                array_ids = (Symbol(datasub[row, col_arrayid]),
                             Symbol(datasub[row + 1, col_arrayid]))
                reaches = getreachtuple(array_ids, paths, arraydict)
                # Get run duration in seconds
                duration = datasub[row + 1, col_time] - datasub[row, col_time] |>
                    x -> x.value / 1000

                if !isnothing(reaches)
                    # Make RunStat
                    rs = RunStat(DateTime(datasub[row, col_time]),
                                 duration,
                                 agent_sym,
                                 reaches, 
                                 -1)
                    # Save to vector
                    push!(rsvec, rs)
                end
            end
        end
    end

    return rsvec
end

"""
    load_detections_arraydict(xml_doc)

For a xml metadata sheet, return a dictionary linking the reciever arrays to the
corresponding reaches in the spatial data {array_id::Symbol => reach_id::Symbol}.

The resulting dictionary is used in the function `load_detections_file`, for
creating a RunStatVector from the detections data.
"""
function load_detections_arraydict(xml::EzXML.Document)

    # Extract list of array nodes from xml doc
    xpath = "/run_covariates/reaches/reach"
    ns_reach = findall(xpath, xml)

    arrdict = Dict{Symbol, Symbol}()

    for n_reach in ns_reach
        sy_arr = Symbol(n_reach["array_id"])
        sy_reach = Symbol(n_reach["reach_geometry_id"])
        arrdict[sy_arr] = sy_reach
    end

    return arrdict

end
