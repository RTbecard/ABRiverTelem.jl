#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell
# =============================================================================
# Function for loading covariate data into memory.
# =============================================================================


# ======================== Exported Functions =================================
"""
    read_covariates_reaches(xml_path, spacedelta)

Load all required covairate data.  Returns CovariateDict object.

Spatial data will be sampled along each reach in intervals of `spacedelta`
meters.
"""
function load_covariates_reaches(
        xmlfile::AbstractString, spacedelta::AbstractFloat)

    # ------ Load xml file
    doc = misc_loadxml(xmlfile)
    path = dirname(xmlfile)

    # ------ Init empty covariate dictionary
    covsdict = CovariateDict()

    # Store node paths
    n_run_covariates = findfirst("/run_covariates", doc)
    n_detections = findfirst("/run_covariates/detections", doc)
    n_geometry = findfirst("/run_covariates/reach_geometry", doc)
    n_global = findfirst("/run_covariates/covariates_global", doc)

    # ------ Get lists of variable names to extract
    variables_reach = load_covariates_getVariables(n_run_covariates)
    variables_global = load_covariates_getVariables(n_global)

    # ============ Load global covariate data
    # Make new Reach entry in covariate dictionary
    global_dict = ReachDict()
    covs_dict[:global] = Reach(reach_dict, Nothing)

    load_covaraites_global!(n_global, global_dict, variables_global,
        n_global["time_column"], n_global["tz"])

    # ============ Loop reaches and load reach datasets
    ns_reach = findall("/run_covariates/reaches/reach", doc)
    @debug string(length(ns_reach), " reaches listed")
    for n_reach in ns_reach
        @debug "Reach: $(n_reach["reach_geometry_id"])"
        # Skip if this node has no upstream geometry
        if Symbol(n_reach["reach_geometry_id"]) == Symbol("NA")
            continue
        end

        # ------ Make new reach dictionary
        # Get length of reach
        lngth = py_getStreamLength(reach_id,
            misc_expandpath(path, n_geometry["file"]),
            n_geometry["id_attribute"])
        # Make new Reach entry in covariate dictionary
        reach_dict = ReachDict()
        covs_dict[Symbol(reach_id)] = Reach(reach_covs, lngth)

        # ------ Load non-spatial convariates for reach
        load_covariates_reach!(n_reach, reach_dict, variables_reach,
                               n_covariates_reaches["time_column"],
                               n_covariates_reaches["tz"])

        # ------ Load spatial covariates for reach
        load_covariates_reach_spatial!(n_reach, reach_dict, spacedelta)

    end

    return covsdict

end

"""
    load_covariates_agents(xmlfile; csvargs...)

Load agent datasheet from disk as a DataFrame.  Additional keyword arguemnts are
passed directly to `CSV.File` (or ignored if a RDS file is specified).


"""
function load_covariates_agents(xmlfile; csvargs...)

    doc = loadxml(xmlfile)
    path = dirname(xmlfile)

    # ======================== Load detections Data ===========================
    xpath = "/run_covariates/detections_meta"
    n_detections_meta = findfirst(xpath, doc)
    if n_detections_meta == nothing
        error("No detections_meta in xml sheet.")
    end
    meta_id = n_detections_meta["agent_id_column"]
    meta_file = joinpath(path, n_detections_meta["file"])
    if !isnothing(n_detections_meta)
        df = readtabular(meta_file; csvargs...)
        if  meta_id ∉ names(df)
            @error """
            Missing id column in detections metadata.

            DataFrame: $(first(df, 6))
            """
            error("Invalid detections metadata tabular file: ", basename(meta_file))
        end
        # Rename agent id column
        rename!(df, meta_id => "agent")
        # Convert agent IDs to symbols
        df[!, :agent] = Symbol.(df[!, :agent])

        return df
    end

end

# =========================== Internal Functions ==============================
"""
    read_covaraites_reach(reach_covs::ReachCovariates, csv_file::AbstractString,
        time_col::AbstractString, vars_dict::AbstractString,
        tz_detections::AbstractString, tz_covarr::AbstractString)

Read a csv file and update `covDict` with the parsed covariate data.

`time_col` is a string holding the name of the csv column holding time data.
Key/value pairs in`vars_dict` indicate the name of the variable to be read,
and the associated column name in the csv file.
"""
function load_covariates_tabular!(reach_covs::ReachDict, csv_file::AbstractString,
        time_col::AbstractString, tz_covarr::AbstractString,
        vars_dict::Dict{Symbol, Symbol},
        tz_detections::AbstractString)

    @debug "Loading file $(basename(csv_file))"

    # ------ Check that time column is present and formatted correctly
    if time_col ∉ names(csv)
        @error """
        Bad DataFrame:\n $(first(csv, 6))
        """
        error("Time column $(time_col) missing from tabular file: $(basename(csv_file))")
    end
    if typeof(csv[!, time_col]) ≠ Vector{DateTime}
        @error """
        If using csv, check formatting of time column.
        Expecting ISO locally formatted times (without timezone)
        Example: 2012-06-21T18:00:00

        Loaded DataFrame:\n $(first(csv, 6))
        """
        error("Invalid time column in tabular file: ", basename(csv_file))
    end
    # Return value if no data is read from file
    covarr = nothing

    # ------ Loop found variable names
    for pair in vars_dict

        # ------ Get name of variable to extract
        var_col = pair.second
        var_name = pair.first

        # ------ Skip this column if already in covariates file
        if var_name ∈ keys(reach_covs)
            continue
        end

        # ------ Make CovariateArray
        @debug "-- Loading column $var_name"
        vec = csv[!, var_col]
        time = csv[!, time_col]
        time_float = datetime2unix.(time)
        # Get minimim time interval (seconds)
        time_deltas = diff(time_float)
        time_delta = minimum(time_deltas)
        # Generate row indexes (allows for missing timesteps in csv)
        rows = ((time_float .- minimum(time_float)) / time_delta) .+ 1
        # Check valid time values
        if any(isinteger.(rows) == false)
            @error """
            Tabular data must be in discrete time.
            Make sure that all timesteps are a multiple of some constant step value.
            """
            error("Continuous time values detected in $(basename(csv_file)).")
        end
        rows = convert.(Int, rows)
        # ------ Store CovariateArray
        # init with missing, then fill in existing values
        mat = Matrix{Union{Float64, Nothing}}(nothing, (maximum(rows), 1))
        mat[rows,:] = vec
        time_start = misc_adjusttimezone(time[1], tz_covarr, tz_detections)
        covarr = CovariateArray(time_start, time_delta, nothing, mat, csv_file)
        reach_covs[var_name] = covarr
    end
end

"""
    load_covariate_reach_nonspatial!(n_reach::Node, reach_dict::Reach)

Load non spatial covariate data for the given reach.

"""
function load_covariates_reach!(n_reach::Node, reach_dict::ReachDict,
        reach_vars::Dict, reach_time_column, reach_tz)

    # Get reqired nodes
    n_detections = findfirst("/run_covariates/detections", document(n_reach))

    # ------ Make copy of vars_dict
    # (we will remove keys from here as we load them from file)
    reach_vars = deepcopy(reach_vars)

    # ============ Load reach constants
    load_covariates_constants!(n_reach, reach_dict)

    # ------ Loop reach covariate files for reach and populate covariate dict
    xpath = "./covariates"
    n_reach_covs = findall(xpath, n_reach)

    @debug "------ Loading reach tabular data, $(length(n_reach_covs)) files listed"

    for n_reach_cov in n_reach_covs
        # Scan all reach covariate files (in node order) until all
        # requested covariates have been read.

        # ============ Load csv data into CovriateDictionary
        # ------ Get file name for covariate csv
        csv_file = misc_expandpath(path, n_reach_cov["file"])
        # ------ Load data
        covarr = load_covariates_tabular!(reach_vars, csv_file,
                                 reach_time_column, reach_tz,
                                 varsdict_reach, n_detections["tz"])

        # ------ Remove read variables
        filter!(x -> x.first ∉ keys(reach_vars), reach_vars)

        # ------ Exit loop if all requested covariates have already been read
        if length(reach_vars) == 0
            break
        end
    end
end

"""
    load_covariate_reach_spatial!(n_reach::Node, reach_dict::Reach)

Load spatial covariate data for the given reach.
"""
function load_covariates_spatial!(
        n_reach::Node, reach_dict::ReachDict, spacedelta::Float64)

    # Get required XML nodes
    n_geometry = findfirst("/run_covariates/reach_geometry", document(n_reach))
    ns_netcdf = findall("/run_covariates/netcdf", document(n_reach))

    # ------ Loop nc files
    @debug "$(length(ns_netcdf)) nc file(s) listed"
    for n_netcdf in ns_netcdf

        # ------ Loop all variables for this nc file
        for n_variable in eachelement(n_netcdf)

            # ------ Extract NetCDF data
            # Make arguments for spatial interpolation function
            args = (n_reach["reach_geometry_id"], spacedelta,
                    misc_expandpath(path, n_geometry["file"]),
                    n_geometry["id_attribute"],
                    misc_expandpath(path, n_netcdf["file"]),
                    n_netcdf["var_x"], n_netcdf["var_y"],
                    n_netcdf["var_time"], n_variable["variable_name"],
                    n_netcdf["bin_mark"], n_netcdf["proj4string"],
                    n_detections["tz"], n_netcdf["tz"])

            @debug """nc file: $(basename(n_netcdf["file"])), reach: , $(n_reach["reach_geometry_id"]), nc variable: $(n_variable["variable_name"]))
            $args"""

            # Check file paths are valid
            for file in [args[3] args[5]]
                abs_path = joinpath(dirname(xmlfile), file)
                if !isfile(abs_path)
                    @error """Provided file path is invalid: \n\n$(abs_path)\n
                    Note that paths listed in the XML metadata file are relative to
                    its current location.
                    """
                    error("Cannot find file specified in XML: ", file)
                end
            end

            # Read nc file
            (nc_arr, time_start, time_step) = py_netcdfSampler(args...)
            # Save results to dictionary
            covarr = CovariateArray(DateTime(time_start),
                                     time_step, spacedelta,
                                     nc_arr, n_netcdf["file"])
            reach_dict[Symbol(n_variable["variable_name"])] = covarr
        end
    end
end

function load_covaraites_global!(n_global::Node, reach_dict::ReachDict,
        global_vars::Dict, global_time_column, global_tz)

    # Get reqired nodes
    n_detections = findfirst("/run_covariates/detections", document(n_reach))

    # ============ Load global constants
    load_covariates_constants!(n_global, reach_dict)

    if n_global["file"] ∉ ["NA", ""]
        # ============ Load csv data into CovariateDictionary
        # ------ Get file name for covariate csv
        csv_file = misc_expandpath(path, n_global["file"])
        # ------ Load data
        covarr = load_covariates_tabular!(reach_vars, csv_file,
                                 global_time_column, global_tz,
                                 global_vars, n_detections["tz"])
    end
end

"""
    load_covariates_getVariables(n)

Extract child variable elemnts.  Return a Dict with label => variable_name
"""
function load_covariates_getVariables(n::Node)

    vars = Dict(Symbol(el["label"]) => Symbol(el["covariate_col"])
                for el in eachelement(n))

    return vars
end

function load_covariates_constants!(n::Node, reach_dict::ReachDict)

    ns_constant = findall("./constant", n)
    for n_constant in ns_constant

        key = Symbol(n_constant["label"])
        val = parse(Float64, n_constant["value"])

        @debug "Loading constant $(key)"

        reach_dict[key] = CovariateArray(nothing, nothing, nothing,
                                         hcat(val), "Constant")
    end
end
