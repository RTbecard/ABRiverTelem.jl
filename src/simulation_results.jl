#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell
# =============================================================================
# Functions for extracting simulation results
# =============================================================================

"""
    results(runstats)

Convert a RunStatVector into a DataFrame of results.
"""
function results(runstats::RunStatVector; df::Union{DataFrame, Nothing} = nothing)

    rows = length(runstats)
    if isnothing(df)
        df = DataFrame(agent = Vector{Symbol}(undef, rows),
                       duration = Vector{AbstractFloat}(undef, rows),
                       reachidx = Vector{Integer}(undef, rows))
    else
        if size(df) != (49, 3)
            error("Invalid dims for provided `df`: Expected ($rows, 3), got $(size(df)).")
        end
        colts = eltype.(eachcol(df))
        if colts != [Symbol, AbstractFloat, Integer]
            error("Invalid column types: Expected DataType[Symbol, AbstractFloat, Integer], got $(colts).")
        end
    end

    for i in 1:rows
        runstat = runstats[i]
        df[i,:] = (runstat.agent, runstat.duration, runstat.reachidx)
    end

    return df
end

"""
    results(runvector)

"""
function results(runvec::RunVector; df::Union{DataFrame, Nothing} = nothing)

    rows = length(runvec)
    if isnothing(df)
        df = DataFrame(agent = Vector{Symbol}(undef, rows),
                       duration = Vector{AbstractFloat}(undef, rows),
                       reachidx = Vector{Integer}(undef, rows))
    else
        if size(df) != (49, 3)
            error("Invalid dims for provided `df`: Expected ($rows, 3), got $(size(df)).")
        end
        colts = eltype.(eachcol(df))
        if colts != [Symbol, AbstractFloat, Integer]
            error("Invalid column types: Expected DataType[Symbol, AbstractFloat, Integer], got $(colts).")
        end
    end

    for i in 1:rows
        r = runvec[i]
        duration = Second(r.state[:time] - r.timestart)
        df[i,:] = (r.agent, duration.value, r.state[:reachidx])
    end

    return df

end

"""
    results(simulation)

Return a matrix with rows equal to the length of runs and columns providing the
distance, duration, and survival state of the completed runs.
"""
function results(sim::Simulation; df::Union{DataFrame, Nothing} = nothing)

    return results(sim.runvec, df = df)

end

