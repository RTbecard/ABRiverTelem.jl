#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell
# =============================================================================
# Functions for defining the state update model
# =============================================================================

"""
    StateModel(expr, β)

A struct holding the parameterization of the migration state model for use 
during simulations.

`β` is a dictionary of parameter names and values.  `expr` is an expression
which has variable names which match either parameter names specified in `β` or 
symbol names in from the CovariateDict object that will be used in the 
simulation.  Use `covaraites()` to see a list of available covariate names that can be used in the expression.

"""

"""Dictionary for holding model parameters"""
βDict = Dict{Symbol, Union{Float64, Dict{Symbol, Float64}}}

struct StateModel
    expr::Expr
    β::βDict
    function RunModel(expr, β)
        chkβ(expr, β)
        new(expr, β)
    end
end

function Base.show(io::IO, ::MIME"text/plain", model::StateModel)
    println(io, "StateModel")
    println(io, "Expression:")
    println(io, quote_string(model.expr))
    str = []
    for x in pairs(model.β)
        if typeof(x.second) <: AbstractDict
            push!(str,
                  string(x.first, " = [", string(length(x.second), " levels]")))
        else
            push!(str, string(x.first, " = ", x.second))
        end
    end
    println(io, "β: ", join(str, ", "))
end

function Base.show(io::IO, model::StateModel)
    print(io, "RunModel")
end

function chkβ(f, β)
    
    vars, factors = eqparser(f)
    for b in β
        if b.first ∉ vars
            error("β[:$(b.first)] is missing from provided expression `f`")
        end
    end
end

"""
    quote_string(expr)

Convtert an expression to a quote.  This will automatically remove commented 
lines from the expression.
"""
function quote_string(expr)
    # Convert to string
    str = string(expr)
    # Replace comments
    str = replace(str, r"\n *#.*\n" => "\n")
    str = replace(str, r"#.*\n" => "\n")
    # Add indentation
    str = replace(str, r"^" => "    ")
    str = replace(str, r"\n" => "\n    ")
end

