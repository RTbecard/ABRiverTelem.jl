function simulation_constructor(runvec, covdct, 
        runprm, prtprm, velprm; meta = nothing)

    if isnothing(meta)
        meta = DataFrame()
    end

    if (nrow(meta) != 0) && length(meta[!, :agent]) ≠ length(unique(meta[!, :agent]))
        dups = []
        for un in unique(meta[!, :agent])
            if count(x -> x == un, meta[!, :agent]) > 1
                push!(dups, un)
            end
        end
        @error """Duplicate agents found in metadata table: $dups"""
        error("Duplicate agents in metadata")
    end

    # ------ Check that run vectors are all in covariate dataset
    # Get reaches from run vectors
    reachtuples = (x -> x.reaches).(runvec)
    rchs = [r for rt in reachtuples for r in rt ]
    rchs_covs = keys(covdct)
    for rch in rchs
        if rch ∉ rchs_covs
            @error """
            Reach $rch from RunVector is missing from CovariateDict

            Reaches in CovariateDict: $(keys(covdct))"""
            error("Mismatched RunVector and CovariateDict")
        end
    end

    # ------ Get symbols from each equation
    p_sym, p_cs = eqparser(prtprm.f)
    r_sym, r_cs = eqparser(runprm.f)
    v_sym, v_cs = eqparser(velprm.f)

    # ------ Convert catagorical variabes into symbols
    cats = unique(vcat(r_cs, p_cs, v_cs))
    for cat in cats
        if cat ∈ names(meta)
            meta[cat] = Symbol.(meta[cat])
        else
            for reach in values(covdct)
                if cat ∈ keys(reach)
                    reach[cat][:,:] = Symbol.reach[cat][:,:]
                end
            end
        end
    end

    # ------ Convert formulata (Expressions) to functions
    p_args_cov = eq_cov_arguments(prtprm, covdct, meta = meta)
    p_args_β = Symbol.(keys(prtprm.β))
    p_func = expr2func(prtprm.f, vcat(p_args_cov, p_args_β))
    r_args_cov = eq_cov_arguments(runprm, covdct, meta = meta)
    r_args_β = Symbol.(keys(runprm.β))
    r_func = expr2func(runprm.f, vcat(r_args_cov, r_args_β))
    v_args_cov = eq_cov_arguments(velprm, covdct, meta = meta)
    v_args_β = Symbol.(keys(velprm.β))
    v_func = expr2func(velprm.f, vcat(v_args_cov, v_args_β))

    return r_func, p_func, v_func
end


"""
    simulation_constructor(runvector, covariatedict, 
        runmodel, prtmodel, velmodel, timestep; meta = nothing)

Initialize a simulation structure.  

`timestep` sets the duration in seconds of each discreete time step of the 
simulation.  CovariateDictionary and meta DataFrame objects can be shared 
between simulations, but all other inputs will be a unique instance for that 
simulation.
"""
struct Simulation
    runvec::RunVector
    covdct::CovariateDict
    meta::Union{DataFrame, Nothing}
    meta_mat::Matrix{Union{Symbol, Float64}}
    stateprm::StateModel
    statefun::Function
    timestep::Float64
    covs_meta::Vector{Symbol}
    covs_covdct::Vector{Symbol}
    function Simulation(runvec::RunVector, covdct::CovariateDict, 
            prtprm::PrtcpModel, runprm::RunModel, velprm::VelocModel,
            timestep::Float64;
            meta::Union{DataFrame, Nothing} = nothing)
        # Generate state functions
        fcns = simulation_constructor(runvec, covdct, 
                                runprm, prtprm, velprm; meta = meta)
        # Error checking
        timestep < 0 && error("Negative timestep provided.")
        # Make metadata matrix
        if isnothing(meta)
            meta = DataFrame()
        end
        meta_mat = Matrix{Union{Symbol, Float64}}(undef, length(runvec), 
                                                  length(covariates(meta)))
        meta2 = deepcopy(meta)
        if nrow(meta) > 0
            # ------ Convert metadata values to float or symbol
            for col in names(meta)
                if eltype(meta[!,col]) <: Number
                    meta2[!, col] = convert.(Float64, meta[!, col])
                else
                    meta2[!, col] = Symbol.(meta[!, col])
                end
            end 
            # ------ Check for missing agents in metadata
            for (i, rn) in enumerate(runvec)
                row = filter(:agent => (x -> x==rn.agent), meta2, view = true)
                if size(row)[1] == 0
                    @error """Agent $(rn.agent) missing from metadata."""
                    error("Invalid metadata.")
                end
                @bp
                meta_mat[i,:] .= Vector(row[1, covariates(meta2)])
            end
        end
        # ------ Create struct
        new(deepcopy(runvec), covdct, meta, meta_mat, 
            deepcopy(prtprm), deepcopy(runprm), deepcopy(velprm), 
            fcns[1], fcns[2], fcns[3], timestep, 
            covariates(meta), covariates(covdct)) 
    end
end

function Base.show(io::IO, ::MIME"text/plain", sim::Simulation)
    println(io, "Simulation")
    prt = length(filter(x -> x.state.participation, sim.runvec))
    txt = ["Participating:" "$(prt)/$(length(sim.runvec))"]
    strt = length(filter(x -> x.state.time > x.timestart, sim.runvec))
    txt = vcat(txt, ["Started Run:" "$(strt)/$(length(sim.runvec))"])
    cmplt = length(filter(x -> x.state.reachidx == -1, sim.runvec))
    txt = vcat(txt, ["Completed run:" "$(cmplt)/$(length(sim.runvec))"])
    pretty_table(io, txt, noheader=true, tf=tf_borderless, alignment = [:r, :l])
end

function Base.show(io::IO, sim::Simulation)
    cmplt = length(filter(x -> x.state.reachidx == -1, sim.runvec))
    print("Simulation[completed $(cmplt)/$(length(sim.runvec))]")
end
