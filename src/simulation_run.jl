#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell

"""
    step(run, timestep, simulation)

Step the agent through space and time.  If the end of the run is reached,
the `state[:reachidx]` will be set to `-1` and the agent will no longer
participate in the simulation.  If the agent goes behind the starting point,
`state[:reachidx]` will be set to `0`.
"""
function step_position(rn::Run, sim::Simulation, covbuff::CovariateBuffer)

    covdct = sim.covdct
    time = sim.timestep
    reach = rn.reaches[rn.state.reachidx]
    state = rn.state
    # Step time
    state.time = state.time + Second(time)
    # Exit if velocity == 0 (agent is stationary)
    if state.velocity == 0
        return
    end
    # get step distance in meters
    state.reachposition = state.velocity*time + state.reachposition
    # Check if stepping into new reach
    #reach_len = covbuff.reachlen
    reach_len = sim.covdct[reach].length
    while state.reachposition >= reach_len || state.reachposition < 0
        # ------ check for end of run
        if state.reachidx == length(rn.reaches)
            state.participation = false
            state.reachposition = 0
            state.reachidx = -1
            state.velocity = 0
            return
        end
        if state.reachposition >= reach_len
            # Move individual to next reach
            state.reachidx += 1
            # Get new reach id
            reach = rn.reaches[state.reachidx]
            # Calculate position in new reach
            state.reachposition -= reach_len
            reach_len = covdct[reach].length
            # Update covariate buffer
            cb_set_reach(covbuff, covdct[reach].reachcovs, reach)
        elseif state.reachposition < 0
            # Move individual to previous reach
            state.reachidx -= 1
            # ------ check for crossing behind starting point
            if state.reachidx == 0
                # End run if agent moves behind start position
                state.participation = false
                state.reachposition = 0
                state.velocity = 0
                return
            else
                reach = rn.reaches[state.reachidx]
                # Calculate position in new reach
                state.reachposition -= reach_len
                reach_len = covdct[reach].length
            end
        end
    end
end


"""
    step_state(rn, sim)

Calculate the next state for the agent.
"""
function step_state(rn::Run, sim::Simulation, covbuff::CovariateBuffer)

    covdct = sim.covdct
    state = rn.state

    ilogit(x) = 1/(1+ℯ^(-x))

    # get reach covairates for current position
    cb_update(covbuff, rn)
    # Calculate new participation state
    prt_state = sim.prtfun(covbuff.v[1,1:covbuff.n[1]]...)
    # Convert to propability (from logit)
    if 0 == rand(D.Binomial(1, ilogit(prt_state)))
        state.participation = false
        state.running = false
        state.velocity = 0
        return
    end
    # Calculate new run state
    run_prob = sim.runfun(covbuff.v[2,1:covbuff.n[2]]...)
    # Convert to probability (from logit)
    state.running = rand(D.Binomial(1, ilogit(run_prob)))
    cb_set_running(covbuff, state.running)
    # Calculate new velocity state
    state.velocity = sim.velfun(covbuff.v[3,1:covbuff.n[3]]...)
    cb_set_velocity(covbuff, state.velocity)
    # Move agent
    step_position(rn, sim, covbuff)
end

function run(sim::Simulation, maxiter::Int64; track = false)

    # Init covariate buffer
    # This holds data required for the state equations on each step
    covbuff = CovariateBuffer(sim)
    # init tracker object
    track && (tracker = Tracker(maxiter, sim.runvec, sim.timestep, covbuff))

    # ============ Loop agents
    #(i, rn) = collect(enumerate(sim.runvec))[4]
    for (i, rn) in enumerate(sim.runvec)
        state = rn.state
        # skip if agent not participating
        !state.participation && continue
        # Get reach id
        reach = rn.reaches[state.reachidx]
        # Initalize covariates for this agent (meta data)
        cb_set_meta(covbuff, sim, i)
        # Initalize the covariates for this reach
        cb_set_reach(covbuff, sim.covdct[reach].reachcovs, reach)
        # ============ Run agent
        stp = 0
        while state.participation && (stp < maxiter)
            @debug """i:$i, step: $(stp); """
            # Calcualte next state of agent
            step_state(rn, sim, covbuff)
            # Increment iteration counter
            stp += 1
            # Save tracking data
            track && update(tracker, stp, i, rn, sim.covdct, covbuff)
        end
    end

    # Return tracking data
    track && (return tracker)

    return nothing
end


"""
    reset(sim, state, running = true)

Resets Runs (agents) to their initial states.  This allows a single RunVector
to be used in repeated simulations.  If the vector of dictonaries `state` is
provided, the runs can be reset to a specified state.  This is useful if you
want your agents to begin at a position or time midway through the run.

By default, agents are initalized in a running state with a velocity of 0 at
the start time and position of the run.
"""
function reset(sim::Simulation; state::Union{RunState, Nothing} = nothing,
        running = true)

    for (i, rn) in enumerate(sim.runvec)
        state = rn.state
        state.reachposition = 0.
        state.reachidx = 1
        state.participation = true
        state.velocity = 0.
        state.running = running
        state.time = rn.timestart

        # Write optional paramter init state
        if !isnothing(state)
            rn.state.reachposition = state.reachposition
            rn.state.reachidx = state.reachidx
            rn.state.participation = state.participation
            rn.state.velocity = state.velocity
            rn.state.running = state.running
            rn.state.time = state.time
        end
    end
end

"""
    setβ(sim, prt = nothing, run = nothing, vel = vel)

Set new β parameters for each model.  Parameters are passed as named tuples.
Error will be thrown if  a unknown parameter name is passed.
"""
function setβ(sim; prt = nothing, run = nothing, vel = nothing)
    if !isnothing(prt)
        for (k,v) in pairs(prt)
            sim.prtprm.β[k] = v
        end
    end
    if !isnothing(run)
        for (k,v) in pairs(run)
            sim.runprm.β[k] = v
        end
    end
    if !isnothing(vel)
        for (k,v) in pairs(vel)
            sim.velprm.β[k] = v
        end
    end
end
