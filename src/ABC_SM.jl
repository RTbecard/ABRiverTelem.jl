struct ABC_SM_result
    θ_vec::Vector{Matrix{Float64}}
    w_vec::Vector{Vector{Float64}}
    ϵ̄::Vector{Float64}
    priors::Union{NamedTuple, Nothing}
    N::Int64
    T::Int64
    ar::Vector{Float64}
    d̄::Vector{Float64}
    Σ_vec::Vector{Matrix{Float64}}
end

function show(io::IO, ::MIME"text/plain", result::ABC_SM_result)
    rows = ["Parameters:",
            "Particles:",
            "Population iterations:",
            "Threshold schedule:",
            "Acceptance rates:"]
    println(io, "ABC_SM")
    pretty_table(io, [join(keys(result.priors), ", ");
                      "$(result.N)";
                      "$(result.T)";
                      join(round.(result.ϵ̄, digits = 3), ", ");
                      join(round.(result.ar, digits = 3), ", ")],
                noheader = true, row_names = rows, tf = tf_borderless)
end

"""
    ABC_SM_model(sim, maxiter, x_obs, f_dist, f_setβ, priors)

Object holding the propreties of the model to apply ABCSM to.
"""
struct ABC_SM_model
    sim::Simulation
    maxiter::Int64
    x_obs::Vector{Float64}
    f_dist::Function
    f_setβ::Function
    function ABC_SM_model(sim, maxiter, x_obs, f_dist, f_setβ)
        new(deepcopy(sim), maxiter, deepcopy(x_obs), f_dist, f_setβ)
    end
end

"""
    abc_sm(sim, maxiter, x_obs, priors, N, ϵ, 
            f_dist, f_setβ;
            θ_init = nothing, w_init = nothing)

Apply sequential Monte Carlo Approximate Baysian Computation to the simulation.
An ABC_SM struct will be returned which can be plotted.

For the pertubation kernel, a multivariate normal kernel with optimal local
covariance matrix is used (Filippi et al. 2013).  The provided threshold ϵ will
be updated on each population sample according to Simola et al. 2020 until

"""
function abc_sm(model::ABC_SM_model, priors::Union{Nothing, NamedTuple}, 
        N::Int, ϵ::Union{Float64, Vector{Float64}};
        write = true, qₜ_thresh = 0.95, ar_thresh = 0.05, T_thresh = nothing,
        append = nothing)

    # Load model variables
    sim = model.sim
    maxiter = model.maxiter
    x_obs = model.x_obs
    f_dist = model.f_dist
    f_setβ = model.f_setβ

    # ------ Init variables ABC variables
    n = length(priors)      # Number parameters
    θₚ = Matrix{Float64}(undef, n, N)   # Previous population of particles
    θ = Matrix{Float64}(undef, n, N)    # Current population of particles
    θ_vec = Vector{Matrix{Float64}}(undef, 0)   # Vector of all previous populations
    β = Vector{Float64}(undef, n)       # Sampled particle (vector of params)
    βₚ = Vector{Float64}(undef, n)       # Sampled particle (vector of params)
    w = Vector{Float64}(undef, N)       # Particle weights
    wₚ  = Vector{Float64}(undef, N)     # Previous particle weights
    w_vec  = Vector{Vector{Float64}}(undef, 0)  # All previous particle weights
    Σ_vec = fill(Matrix{Float64}(undef, n, n), N)  # Local cov.mat. for all particles
    x_sim = Vector{Float64}(undef, length(sim.runvec)) # Simulated sum.stat.
    ϵ̄ = Vector{Float64}(undef, 0) # Vector of threshold values
    n_sim_pop = 0       # Simulation counter for populations
    n_sim = 0           # Simulation counter
    d_prior = nothing   # Prior density
    accept_ratio = Vector{Float64}(undef, 0)  # Vector for acceptance ratio
    t = 1::Int64 # Population counter
    result = nothing    # ABC results
    d̄ = Vector{Float64}(undef, N) # Vector of accepted distances

    # Init population loop
    if !isnothing(append)
        println("Continuing from previous run")
        println("Using previously definied priors")
        θₚ = append.θ_vec[end]
        wₚ = append.w_vec[end]
        θ_vec = append.θ_vec
        w_vec = append.w_vec
        ϵ̄ = append.ϵ̄
        t = append.T + 1
        accept_ratio = append.ar
        priors = append.priors
    else
        if isnothing(priors)
            error("Priors are missing.")
        end
    end
    priors_t = tuple(priors...)
    
    # ------ S 2.0
    println("Starting ABCSM for $n parameters and $N particles.")
    # Timer for whole algorithem
    clk_all = now()
    # Timer for displaying updates
    clk_update = now()
    # --- Loop populations
    while true
        # Timer for population loop
        clk_pop = now()
        @printf " --- Population %i, ϵ = %.3f ---\n" t ϵ
        # Reset simulation counter for this population
        n_sim_pop = 0
        # --- Loop/sample particles for new population
        for i in 1:N
            # --- Particle sample loop
            while true
                # ------ S 2.1
                if t == 1
                    # --- Sample particle from prior
                    β .= _r_prior(priors_t)
                else
                    # --- Sample particle from previous population
                    βₚ .= _r_population(θₚ, wₚ, N)
                    # --- Perturb samples
                    # Pertubation kernel (MVGaussian) covariance
                    Σ_vec[i] .= _Σ(βₚ, wₚ, θₚ, n, N)
                    # Sample from MvNormal pertubation kernel
                    β .= rand(MvNormal(βₚ, Σ_vec[i]))
                end
                # --- Evaluate prior density
                # Restart loop if zero density is sampled.
                d_prior = _d_prior(priors_t, β, n)
                d_prior == 0 && continue
                # ------ Status update every 10 seconds
                if (now() - clk_update).value > 10000
                    clk_update = now()
                    txt = string(
                            @sprintf("Total sims: %5i; Population sims: %5i; ",
                                     (n_sim + n_sim_pop), n_sim_pop),
                            @sprintf("Particle: %5i; Acceptance rate: %5.3f; ",
                                     i, (i-1)/n_sim_pop),
                            @sprintf("Runtime: %4.1f minutes; ",
                                     (now() - clk_all).value/(1000*60)),
                            @sprintf("%5.2f sim/s.",
                                     n_sim_pop/((now() - clk_pop).value/1000)))
                    print("\e[2K") # clear whole line
                    print("\e[1G") # Reset cursor
                    print(txt)
                end
                # --- Exit algorithem if ar_threshold is passed
                if n_sim_pop > 1000 && (i-1)/n_sim_pop < 0.05
                    @printf("Acceptance rate %.4f too low, exiting early...\n",
                           accept_ratio)
                    return result
                end
                # --- Evaluate distance
                # Run simulation
                reset(sim)
                f_setβ(sim, NamedTuple{keys(priors)}(β))
                run(sim, maxiter)
                n_sim_pop += 1
                # Calculate distance
                d = f_dist(sim, x_obs)
                # If particle distance below threshold (accepted), exit sample loop
                if d < ϵ
                    d̄[i] = d
                    break
                end
            end
            # ------ S 2.2
            # Save particle to population
            θ[:,i] .= β
            # --- Assign weights
            if t == 1
                # Equally weighted if from prior
                w[i] = 1 
            else
                # Calculate sampling density
                w[i] = d_prior / _d_sample(β, θₚ, wₚ, Σ_vec, N, n)
            end
        end
        print("\e[2K") # clear whole line
        print("\e[1G") # Reset cursor
        @printf "Population acceptance rate: %.3f.\n" N/n_sim_pop
        # --- Report Simualtion status
        n_sim += n_sim_pop
        # ------ S 3
        # Normalize weights
        w = w / sum(w)
        push!(ϵ̄, ϵ)
        push!(accept_ratio, N/n_sim_pop)
        push!(θ_vec, deepcopy(θ))
        push!(w_vec, deepcopy(w))
        # Store iteration results
        result = ABC_SM_result(θ_vec, w_vec, ϵ̄, priors, N, 
                               t, accept_ratio, d̄, Σ_vec)
        # Write results to disk
        _store_result(result)
        # Update adaptive threshold
        if t ≥ 2
            ϵ, qₜ = _ϵ(θ, w, θₚ, wₚ, ϵ, d̄, qₜ_thresh)
            @printf "Updating distance threshold: ϵ = %.3f; qₜ = %.3f\n" ϵ qₜ 
        end
        # Iterate population vectors
        θₚ .= θ
        wₚ .= w
        # Print timers
        @printf("Population processing time: %.2f minutes\n",
                (now() - clk_pop).value/(1000*60))
        print("Mean posterior values: ")
        means = [@sprintf("%s: %.3e", keys(priors)[x], mean(θ[x,:])) for x in 1:n]
        println(join(means, ", "))
        
        # Check stop conditions
        # t ≥ 3 && qₜ > qₜ_thresh && break
        !isnothing(T_thresh) && t >= T_thresh && break
        # Increment population counter
        t += 1
    end
    return result
end

# ------ Conveniance functions
"""Prior density for particle β"""
function _d_prior(priors_t, β, n)
    return prod((x -> pdf(priors_t[x], β[x])).(1:n))
end
"""Sample particle from prior distribution"""
function _r_prior(priors_t)
    return (x -> rand(x)).(priors_t)
end
"""Weighted sample of particle from population"""
function _r_population(θₚ, wₚ, N)
    i = sample(collect(1:N), ProbabilityWeights(wₚ))
    return θₚ[:, i]
end
"""Calculate weighted covariance of particle population θₚ, centered on β"""
function _Σ(β, wₚ, θₚ, n, N)
    # Init output matrix
    out = zeros(n, n)
    # Calculate bias term
    m = zeros(n)
    for i in 1:N
        m += wₚ[i]*θₚ[:,i]
    end
    # Calculate biaseed weighted sample covariance term
    # https://stats.stackexchange.com/questions/61225/correct-equation-for-weighted-unbiased-sample-covariance
    for i in 1:N
        # Not sure why... but the first form of this equation results in
        # non-hermatian issues.  Assuming its some rounding error.
        #out += wₚ[i] * (θₚ[:,i] - β) * transpose(θₚ[:,i] - β)
        out += wₚ[i]*((θₚ[:,i] - m)*transpose(θₚ[:,i] - m) +
                       (m - β)*transpose(m - β))
    end
    return out
end
"""Calculate density of sample & pertubation distributions"""
function _d_sample(β, θₚ, wₚ, Σ_vec, N, n)
    dens = 0
    for i in 1:N
        βₚ = θₚ[:,i]
        # Sample from MvNormal pertubation kernel
        dens += wₚ[i] * pdf(MvNormal(Σ_vec[i]), β - βₚ)
    end
    return dens
end
function _store_result(res)
    fname = string("ABCSM_",
                   Dates.format(now(), "yyyymmddTHHMMSS"),
                   ".jls")
    Serialization.serialize(fname, res)
end
function _ϵ(θ, w, θₚ, wₚ, ϵ, d̄, qₜ_thresh)
    N = size(θ)[2]
    # ------ Get function for density ratios
    # Current posterior sample
    i_nu = sample(collect(1:N), ProbabilityWeights(w), N)
    x_nu = [tuple(θ[:, i]...) for i in i_nu]
    # Previous posterior sample
    i_de = sample(collect(1:N), ProbabilityWeights(wₚ), N)
    x_de = [tuple(θₚ[:, i]...) for i in i_de]
    # Calculate density ratio function from samples
    r = densratiofunc(x_nu, x_de, KLIEP())
    # ------ Find maximum density ratio
    # Use mean aprams as starting value
    x0 = mean(θ, dims = 2)
    res = optimize(x -> -r(x), x0)
    x_min = res.minimizer
    # Return inverse of max ratio, qₜ
    if isnan(res.minimum)
        println("Failed to converge posterior density ratio. Setting qₜ to threhsold value.")
        qₜ= qₜ_thresh
    else
        qₜ = maximum([0, minimum([-1/res.minimum, 1])])
    end
    #return quantile(d̄, qₜ), qₜ
    #return quantile(d̄, maximum([0.5, qₜ])), qₜ
    return quantile(d̄, 0.5), qₜ
end

"""
    distthresh(sim, N, priors, x_obs, q)

Get inital threshold for distance function.

`N` samples will be be taken from tuple of prior distributions `priors`.  The
thresold will be calculeted based on the provided quantile `q` of resulting 
distance values.

setβ(sim, β) is a function which takes parameter values and applies them to
the model.  Paramters are passed as a named tuple.
"""
function abc_dist_init(model::ABC_SM_model, N::Int64, 
        priors::NamedTuple, q::Float64)
   
    sim = model.sim
    maxiter = model.maxiter
    x_obs = model.x_obs
    f_setβ = model.f_setβ
    f_dist = model.f_dist

    # Init vector for resulting distances
    dists = Vector{Float64}(undef, N)
    # Loop and generate particles
    @showprogress 1 "Sampling..." for i in 1:N
        # Init simulation
        reset(sim)
        β = NamedTuple{keys(priors)}((x -> rand(x)).(values(priors)))
        f_setβ(sim, β)
        # Run simulation
        run(sim, maxiter)
        # Extract distance
        dists[i] = f_dist(sim, x_obs)
    end
    
    out = quantile(dists, q)
    
    v = [0.05, 0.25, 0.50, 0.75, 0.95]
    println("Distance quantiles:")
    for i in length(v):-1:1
        println("q = $(v[i]): $(quantile(dists, v[i]))")
    end
    println("ϵ: $out")

    return out

end

function plot(result::ABC_SM_result; params = nothing, bins = 30, 
        particle = nothing)

    # --- Extract variables from results
    θ = result.θ_vec[end]
    w = result.w_vec[end]
    θₚ = result.θ_vec[end-1]
    wₚ = result.w_vec[end-1]
    n = size(θ)[1]
    N = result.N
    T = result.T
    priors = result.priors
    d̄ = result.d̄
    Σ_vec = result.Σ_vec
    # --- Calcualte mean posteriors
    θ_mean = hcat([ mean(result.θ_vec[t], dims = 2) for t in 1:T ]...)

    # --- Get perturbance kernel if required
    if !isnothing(particle)
        β = θ[:, particle]
        Σ = _Σ(β, w, θ, n, N)
        d_perturb = MvNormal(β, Σ)
    end
    # --- Get parameters to plot
    if isnothing(params)
        params = keys(priors)
    end
    param_i = [ findfirst(x -> x == param, keys(priors)) for param in params]

    # Only support if more than 1 parameter to plot right now
    length(params) <= 1 && (return)
   
    # --- Make varaible indexes for plots
    plots = []
    for i in 1:(length(params) - 1)
        for j in (i+1):length(params)
            push!(plots, (i, j))
        end
    end
    # --- Figure parameters
    n_plots = length(plots) 
    cols = ceil(n_plots^0.5)
    rows = ceil(n_plots/cols)
    # Sample priors
    priors_sample = []
    function sample_points(d, bins)
        x = (collect(0:(bins - 1)) / (bins-1)) .- 0.5
        q1 = quantile(d, 0.001)
        q2 = quantile(d, 0.999)
        center = mean([q1,q2])
        out = x*(q2 - q1) .+ center
    end
    function edges(d, bins)
        x = (collect(0:(bins - 1)) / (bins-1)) .- 0.5
        q1 = quantile(d, 0.001)
        q2 = quantile(d, 0.999)
        df = (q2 - q1) / (bins - 1)
        center = mean([q1,q2])
        out = x*(q2 - q1) .+ center
        push!(out, out[end] + df)
        return out .- 0.5*df
    end
    cont_prior = function(x, y, x_d, y_d, bins)
        D = Matrix{Float64}(undef, bins, bins)
        for i_x in 1:bins, i_y in 1:bins
            D[i_x, i_y] = pdf(x_d, x[i_x]) * pdf(y_d, y[i_y])
        end
        return x, y, D
    end
    cont_pert = function(x, y, d_perturb, x_i, y_i, bins, β)
        D = Matrix{Float64}(undef, bins, bins)
        β_samp = deepcopy(β)
        for i_x in 1:bins, i_y in 1:bins
            β_samp[x_i] = x[i_x]
            β_samp[y_i] = y[i_y]
            D[i_x, i_y] = pdf(d_perturb, β_samp)
        end
        return x, y, D
    end
    
    (i, (i_1, i_2) ) =  collect(enumerate(plots))[1]

    # --- Init new figure
    f = Figure()
    
    for (i, (i_1, i_2) ) in enumerate(plots)
    
        # Indexes fror priors tuple
        ii_1 = param_i[i_1]
        ii_2 = param_i[i_2]
        # Histogram edges
        edges_x = edges(priors[ii_1], bins)
        edges_y = edges(priors[ii_2], bins)
        # Contour points (histogram bin centers)
        c_x = sample_points(priors[ii_1], bins)
        c_y = sample_points(priors[ii_2], bins)
        # Parameter names
        param_1 = params[i_1]
        param_2 = params[i_2]
        # --- Scene setup
        f_row = Int64(ceil(i / cols))
        f_col = Int64(i - (f_row - 1)*cols)
        ax = Axis(f[f_row, f_col])
       
        # --- Plot prior contours
        args = cont_prior(c_x, c_y, priors[ii_1], priors[ii_2], bins)
        cntr_pr = contour!(args..., colormap = :amp)
        ax.xlabel = string(param_1)
        ax.ylabel = string(param_2)
       
        # --- Plot posterior contours
        x = θ[ii_1, :]
        y = θ[ii_2, :]
        hist_po = fit(Histogram, (x, y), (edges_x, edges_y))
        cntr_po = contour!(c_x, c_y, hist_po.weights, colormap = :algae)
        
        # --- Plot posterior trajectory
        lines!(θ_mean[ii_1,:], θ_mean[ii_2,:], color = :mediumseagreen)

        if !isnothing(particle)
            # --- Plot perturbance kernel
            args = cont_pert(c_x, c_y, d_perturb, ii_1, ii_2, bins,  β)
            cntr_pt = contour!(args..., colormap = Reverse(:ice))
        end

    end

    # Add legend
    elem_1 = [LineElement(color = :red)]
    elem_2 = [LineElement(color = :green)]
    elem_3 = [LineElement(color = :blue)]
    lns = [elem_1, elem_2]
    nms = ["Prior", "Posterior"]
    if !isnothing(particle)
        push!(lns, elem_3)
        push!(nms, "Perturbation")
    end
    Legend(f[Int64(rows) + 1, :], lns, nms, tellheight = true, 
           tellwidth = false, orientation = :horizontal)
    # Title
    f[0,:] = Label(f, @sprintf("Population %i, ϵ = %.3f", T, result.ϵ̄[end]),
                  tellwidth = false, tellheight = true)
    
    return f
end
