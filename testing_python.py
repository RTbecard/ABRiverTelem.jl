from os import path
import logging as log
import importlib

# import stream interpolation code
from src import interpolate_streams as istr

# ====================== Testing NetCDF4 Reading ==============================

stream_id = "129659503_2012_6"
distance_step = 100 # meters
pth = "/home/james/Repos/ribes-esr02_datacuration/data/CCV-JSATS"
stream_file = path.join(pth, "stream_pieces_2012.gpkg")
stream_id_attribute = 'id'
nc_file = path.join(pth, "solarRadiation_2012_start-2011-12-31T01:00:00_step-3600s.nc")
nc_var_x = "longitude"
nc_var_y = "latitude"
nc_var_time = "time"
nc_var_sample = "ssrd"
nc_bin_mark = "end"
nc_crs_proj4 = "+proj=longlat +datum=WGS84"
tz_detections = "-08:00"
tz_nc = "+00:00"

# Setup logging
log.basicConfig(level=log.INFO)
lgr = log.getLogger()
lgr.setLevel(log.INFO)
#log.getLevelName(lgr)


importlib.reload(istr)
d_res = istr.netcdfSampler(stream_id, distance_step,
        stream_file, stream_id_attribute,
        nc_file, nc_var_x, nc_var_y, nc_var_time, nc_var_sample, nc_bin_mark,
        nc_crs_proj4, tz_detections, tz_nc)

from matplotlib import pyplot as ply
import datetime
import numpy as np
import xarray

start = datetime.datetime.fromisoformat(d_res['time_start'])
step = d_res['time_step']
arr = d_res['covariate_array']

# ------ Extract all moments in time for a single location
# Get y axis
y = np.array(arr[:, 0])
y.size

# get x axis
steps = np.empty(y.size, dtype="timedelta64[s]")
steps[:] = np.arange(0, step*y.size, step = step)
x = np.full(y.size, np.datetime64(start)) + steps

fig = ply.figure()
ax = fig.add_subplot()
ax.plot(x, y)
ax.set_title("Extracted NetCDF4 Data")
fig.show()
