# Data Structure

ABRiverTelem.jl handles covariate data on four different scopes:

- **global**: Covariates which can be accessed by all agents.
- **reach**: Those covariates which are local to a specific reach.
- **spatial**: Spatially dependant covariates.  These are spatial rasters which
    are sampled by the provided reach geometry data.
- **agent**: Time invariant attributes of the agent (e.g. fork length).

The first three scopes allow for time-variant or -invariant covariate data.
Covariates from all four scopes may be used in the *state transition expression*
governing the behaviour of the agents.

In the language of ABRiverTelem.jl, a **reach** is a *span of river between two
subsequent detection stations*.  Each reach is bound to a single **detection
station** located at the end of the reach.  For downstream migrating fish, this
would be the station located at the downstream end of the reach.  This choice of
upstream or downstream station bindings is arbitrary however, as it won't affect
the simulation procedure.

A *series of subsequent reaches* is a termed a **path**.  Specifying paths allows us
to handle missed detections during a migration run.  When loading detection
data, if an individual has a missed detection during its run, the path
specification will let ABRiverTelem.jl know which detection arrays were missed,
then load the correct covariate data for the reaches in between the detected
stations.

For the processing of spatial data, a geopackage file must be provided.  This
should contain a series of SINGLELINE features, where each feature represents a
reach with an attribute containing a unique identifier string.  These
SINGLELINEs are used to sample from the spatial covariate rasters prior to
running the simulations.  The spatial covariate rasters must be provided in
NetCDF4 format with dimensions encoding x and y spatial dimensions, along with
time formatted as seconds since the UNIX epoch (1970-1-1).

The tabular detections and covariate data files may all have different time
zones.  ABRiverTelem.jl will standardize all data to the zone of the detections
data.  Likewise, NetCDF4 and geopackage files may all have different
projections.  The stream geometry data will be reprojected to the specified
NetCDF4 file prior to sampling.

## XML Metadata

ABRiverTelem.jl attempts to automate as much data preprocessing as possible.  To
do so, it expects an `xml` sheet which provides the location and format of all
the component datasets which will be used in the simulations.  This metadata
sheet has strict formatting requirements which we'll detail below.

All data is contained in the root node `<run_covaraites>` which has the
attribute `name`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<run_covariates name="2016">

</run_covariates>
```

In order, the child elements — along with their attributes — of this node
should be:

- `<detections>`: **Table of detections data**.
   - `file`: Relative path to `csv` or `rds` file.
   - `time_column`: Column name holding ISO formatted times (without time zone).
   - `tz`: Timezone formatted as a UTC offset (e.g. `-08:00`).
   - `station_id_column`: Column holding the detection station identifier.
   - `agent_id_column`: Column holding the agent identifier.
- `<covariates_global>`: **Global scoped covariate data**.
   - `file`: Relative path to `csv` or `rds` file.
   - `time_column`: Column name holding ISO formatted times (without time zone).
   - `tz`: Timezone formatted as a UTC offset (e.g. `-08:00`).
- `<covariates_reaches>`: **Reach scoped covariate date**.
   - `time_column`: Column name holding ISO formatted times (without time zone).
   - `tz`: Timezone formatted as a UTC offset (e.g. `-08:00`).
- `<covariates_netcdf>`: **Spatial scoped covariate data**.
   - `file`: Relative path to netCDF4 file.
   - `var_x`: Name of dimension holding longitude data.
   - `var_y`: Name of dimension holding latitude data.
   - `var_time`: Name of dimension holding time data.
   - `tz`: Timezone formatted as a UTC offset (e.g. `-08:00`).
   - `proj4`: proj4string describing the projection of the raster.
   - `bin_mark`: "start", "mid", or "end", indicating the alignment of time data
       with each raster bin.
- `<covariates_agents>`: **Agent scoped covariate data**.
   - `file`: Relative path to `csv` or `rds` file.
   - `agent_id_column`: Column holding the agent identifier.
- `<reach_geometry>`: **Geopackage holding the reach geometry**.
   - `file`: Relative path to geopackage file.
   - `id_attribute`: Name of attribute holding the `reach_geometry_id`.
- `<reaches>`: **List of reaches along with their reach scoped covariate data
    files.**.
- `<paths>`: **Path definitions**.

While the order of these elements is strict, the inclusion of any of the
covariate elements is optional.

For elements linking to tabular datasets of covariate data (`<covariates_global>`,
`<covariates_reaches>`, `<covariates_agents>`), the covariate columns to be
imported are specified with a child element `variable`.  This has attributes
which describe the resulting name of the imported variable, `label`, and the
name of the table's column holding its data, `covariate_col`.

```xml
  <covariates_reaches time_column="time", tz="-08:00">
    <variable label="discharge" covariate_col="discharge"/>
    <variable label="temperature" covariate_col="temp"/>
    <variable label="FNU" covariate_col="turbidity"/>
  </covariates_reaches>
```

Reach covariate data files are specified as child elements of `<reaches>` named
`<reach>`.  They have the attributes `station_id` and `reach_geometry_id`.  Note
that the first station in a path has no associated SINGLELINE geometry, so
`"NA"` should be provided for these stations.  The associated covariate data
files are provided by `<covariates>` elements with a single attribute, `file`.

```xml
  <reaches>
    <reach station_id="234" reach_geometry_id="NA">
      <covariates file="USGS_2016_11370500.csv"/>
      <covariates file="USGS_2016_11377100.csv"/>
    </reach>
    <reach station_id="233" reach_geometry_id="113875979_2016_233">
      <covariates file="USGS_2016_11370500.csv"/>
      <covariates file="USGS_2016_11377100.csv"/>
    </reach>
  <reaches/>
```

For time-invariant covariate data, they can be directly encoded into the XML
sheet with `<constant>` nodes as either global or reach scoped covariates.
Just be careful about the ordering: `<constant>` elements should always come
before`<covariates>` elements.

```xml
  <reaches>
    <reach station_id="234" reach_geometry_id="NA">
      <constant label="c" value="21.756">
      <covariates file="USGS_2016_11370500.csv"/>
    </reach>
```

```xml
  <covariates_global time_column="time", tz="-08:00">
    <constant label="c" value="21.756">
    <variable label="sun_azimuth" covariate_col="deg"/>
  </covariates_global>
```

Finally, unique paths are defined by `<path>` elements which further contain
`<station>` elements providing an ordered list of detection stations along a
unique path.

```xml
  <paths>
    <path id="0001">
      <station id="234"/>
      <station id="233"/>
      <station id="232"/>
    </path>
    <path id="0002">
      <station id="213"/>
      <station id="212"/>
    </path>
  </paths>
```

ABRiverTelem.jl uses a DTD file to validate the formatting of the provided xml
files.  You can find this file in `./extra/covariatemeta.dtd`.  Improperly
formatted xml files should give an informative error message.

## Loading Data

Provided with a properly formatted xml metadata sheet, `load_covariates_reaches`
will import and preprocess all the component datasets.  The imported data is
stored as a `CovariateDict` object.

```julia-repl
julia> xml_file = "~/Documents/Data/covariates.xml"
julia> covs_dict = load_covariates_reaches(xml_file, 100.0)
Covariate Dictionary
            Reach id   Length (m)                                                                                                                   Variables

  113875979_2020_517       9438.8    discharge:(4175, 1):USGS_2020_11447650.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 95):solarRadiation_2020.nc
  113875979_2020_525       4824.5    discharge:(4175, 1):USGS_2020_11425500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 49):solarRadiation_2020.nc
  153036500_2020_499      35627.3   discharge:(4176, 1):USGS_2020_11313315.csv, FNU:(4176, 1):USGS_2020_11313315.csv, ssrd:(8808, 357):solarRadiation_2020.nc
   43039263_2020_506        702.4     discharge:(4176, 1):USGS_2020_11337190.csv, FNU:(4176, 1):USGS_2020_11337190.csv, ssrd:(8808, 8):solarRadiation_2020.nc
  113875979_2020_534      11441.2   discharge:(4175, 1):USGS_2020_11390500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 115):solarRadiation_2020.nc
   43039263_2020_496      14023.0   discharge:(4176, 1):USGS_2020_11304810.csv, FNU:(4176, 1):USGS_2020_11304810.csv, ssrd:(8808, 141):solarRadiation_2020.nc
  129659741_2020_547      16808.2                                         discharge:(4175, 1):USGS_2020_11390000.csv, ssrd:(8808, 169):solarRadiation_2020.nc
  113875979_2020_529      23111.7   discharge:(4175, 1):USGS_2020_11425500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 232):solarRadiation_2020.nc
  113875979_2020_527       4900.9    discharge:(4175, 1):USGS_2020_11425500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 50):solarRadiation_2020.nc
  113875979_2020_578       8506.3    discharge:(4175, 1):USGS_2020_11370500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 86):solarRadiation_2020.nc
  113875979_2020_523       5818.9    discharge:(4175, 1):USGS_2020_11425500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 59):solarRadiation_2020.nc
  113875979_2020_528       4691.8    discharge:(4175, 1):USGS_2020_11425500.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 47):solarRadiation_2020.nc
  113875979_2020_520       3856.8    discharge:(4175, 1):USGS_2020_11447650.csv, FNU:(4175, 1):USGS_2020_11447650.csv, ssrd:(8808, 39):solarRadiation_2020.nc
  129928336_2020_560       7245.5                                          discharge:(4175, 1):USGS_2020_11381500.csv, ssrd:(8808, 73):solarRadiation_2020.nc
          ⋮                ⋮                                                                    ⋮
                                                                                                                                                60 rows omitted
```

## Accessing Data

Typically you won't need to directly access the underlying data structures that
ABRiverTelem.jl uses.  However, in the case of generating synthetic datasets
this may be a more convenient way to do things.

All covariate data are stored as a `struct` type, `CovariateArray`, with the
following fields:

- `CovariateArray`
  - `timestart`: Starting time (seconds since 1970-1-1).
  - `timedelta`: Time step in seconds.
  - `distancedelta`: Sampling resolution for spatial data, in meters.
  - `data`: An $n$ by $m$ array indicating discrete steps in time and space, respectively.
  - `sourcefile`: String indicating which file this data was loaded from.

For time- or space- invariant covariates, the `data` field will only have a
single row or column, respectively.  Hence, constant covariate values are stored
as $1$ by $1$ arrays.

The function `get()` will extract covariate data from the array.  `get()`
will take arguments indicating the time and reach distance (meters since the
start of the reach) and extract the appropriate element from `data`.

The rows and columns indicate steps through discrete time and space,
respectively.
