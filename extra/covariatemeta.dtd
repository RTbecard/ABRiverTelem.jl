<!ATTLIST run_covariates name CDATA #REQUIRED>
<!ELEMENT run_covariates (detections, covariates_global?, covariates_reaches?,
	covariates_netcdf?, covariates_agents?,
	reach_geometry, reaches, paths)>

<!-- Detections -->
<!ATTLIST detections file CDATA #REQUIRED>
<!ATTLIST detections time_column CDATA #REQUIRED>
<!ATTLIST detections tz CDATA #REQUIRED>
<!ATTLIST detections station_id_column CDATA #REQUIRED>
<!ATTLIST detections agent_id_column CDATA #REQUIRED>
<!ELEMENT detections EMPTY>

<!-- Non-spatial global covariates names -->
<!ATTLIST covariates_global file CDATA #REQUIRED>
<!ATTLIST covariates_global time_column CDATA #REQUIRED>
<!ATTLIST covariates_global tz CDATA #REQUIRED>
<!ELEMENT covariates_global (constant*, variable*)>

<!-- Non-spatial reach covariates names -->
<!ATTLIST covariates_reaches time_column CDATA #REQUIRED>
<!ATTLIST covariates_reaches tz CDATA #REQUIRED>
<!ELEMENT covariates_reaches (variable*)>

<!-- Spatial reach covariates -->
<!ATTLIST covariates_netcdf file CDATA #REQUIRED>
<!ATTLIST covariates_netcdf var_x CDATA #REQUIRED>
<!ATTLIST covariates_netcdf var_y CDATA #REQUIRED>
<!ATTLIST covariates_netcdf var_time CDATA #REQUIRED>
<!ATTLIST covariates_netcdf tz CDATA #REQUIRED>
<!ATTLIST covariates_netcdf proj4string CDATA #REQUIRED>
<!ATTLIST covariates_netcdf bin_mark CDATA #REQUIRED>
<!ELEMENT covariates_netcdf (variable*)>

<!-- Variable element (names of covariates to extract from files) -->
<!ATTLIST variable label CDATA #REQUIRED>
<!ATTLIST variable covariate_col CDATA #REQUIRED>
<!ELEMENT variable EMPTY>

<!-- Agent covariates (optional) -->
<!ATTLIST covariates_agents file CDATA #REQUIRED>
<!ATTLIST covariates_agents agent_id_column CDATA #REQUIRED>
<!ELEMENT covariates_agents EMPTY>

<!-- Reach Geometry -->
<!ATTLIST reach_geometry file CDATA #REQUIRED>
<!ATTLIST reach_geometry id_attribute CDATA #REQUIRED>
<!ELEMENT reach_geometry EMPTY>

<!-- Reaches collection -->
<!ELEMENT reaches (reach+)>

<!-- Individual Reaches -->
<!ATTLIST reach station_id CDATA #REQUIRED>
<!ATTLIST reach reach_geometry_id CDATA #REQUIRED>
<!ELEMENT reach (constant*,covariates*)>

<!ATTLIST covariates file CDATA #REQUIRED>
<!ELEMENT covariates EMPTY
>
<!ATTLIST constant label CDATA #REQUIRED>
<!ATTLIST constant value CDATA #REQUIRED>
<!ELEMENT constant EMPTY>

<!-- Paths -->
<!ELEMENT paths (path+)>

<!ATTLIST path id CDATA #REQUIRED>
<!ELEMENT path (station+)>

<!ATTLIST station id CDATA #REQUIRED>
<!ELEMENT station EMPTY>
