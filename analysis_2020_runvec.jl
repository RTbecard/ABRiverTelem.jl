# ------ Count reach passes 
df_rch = DataFrame(reach = vcat((x -> [x.reaches...]).(runstats)...))
smry_rch = combine(groupby(df, [:reach]), nrow => :count)
transform!(smry_rch, :reach => ByRow((x -> covs_dict[x].length)) => :length)
print(smry_rch)
# ------ Count agent passes
df_agn = DataFrame(agent = vcat((x -> x.agent).(runstats)...))
smry_agn = combine(groupby(df_agn, [:agent]), nrow => :count)
sort!(smry_agn, order(:count), rev = true)
# ------ Filter agents with less than 4 reaches
smry_agn_sub = subset(smry_agn, :count => ByRow(>=(4)))
print(smry_agn_sub)
runstats_filt = filter(x -> x.agent in smry_agn_sub[!, :agent], runstats)
# ------ Return runvector
runvec = initruns(runstats_filt)
reaches = tuple(unique(vcat((x -> [x.reaches...]).(runstats_filt)...))...)
