# ------ Set priors
c = NamedTuple{reaches}(tuple([x = Normal(-2, 6) for x in reaches]...))
priors = (b0_r = Normal(0, 1e1), b0_h = Normal(0, 1e1), 
          b1 = Normal(0, 1e2), b2 = Normal(0, 1e2), b3 = Normal(0, 1e2), 
          c = c)
hyperpriors = ()
# ------ Summary statistics
x_obs = (x -> x.duration).(runstats_filt)
# --- Distance function
function f_dist(sim::Simulation, x_obs::Vector{Float64})
    # Get run time in seconds
    x_sim = (x -> (x.state.time - x.timestart).value/1000).(sim.runvec)
    return norm(x_sim .- x_obs)
end
# --- Function for updating model parameters
function f_setβ(sim, β)
    run_i = 1:5
    #vel_i = 6
    run_t = NamedTuple{keys(β)[run_i]}(values(β)[run_i])
    #vel_t = NamedTuple{keys(β)[[vel_i]]}(10^values(β)[vel_i])
    #setβ(sim; run = run_t, vel = vel_t)
    setβ(sim; run = run_t)
end

model = ABC_SM_model(sim, max_iter, x_obs, f_dist, f_setβ)
