using Revise
using ABRiverTelem
using Debugger
using DataFrames
using Serialization
import GZip
using Printf
using Dates
using Distributions
# ============ Load covariate data 
xml_file = "~/Repos/ribes-esr02_datacuration/data/CCV-JSATS/covariates_2020_final.xml"
covs_dict = readcovs(xml_file, 100.0)
# ========== Create delta ssrd data
for rch in keys(covs_dict)
    reachcovs = covs_dict[rch].reachcovs
    local ssrd = reachcovs[:ssrd]
    local offset = Second(ssrd.timedelta/2)
    local dssrd = CovariateArray(ssrd.timestart + offset, 
                                 ssrd.timedelta, 
                                 ssrd.distancedelta,
                                 diff(ssrd.data, dims = 1),
                                 "Generated")
    reachcovs[:dssrd] = dssrd
end
# show reaches
print(keys(covs_dict))
# ============ Create runvectors for simulated fish
rv = Symbol.(["113875979_2020_538", 
                "113875979_2020_537",
                "113875979_2020_534",
                "113875979_2020_531"])
N = 25
runvec = Vector(undef, N*4)
for i in 1:N
    agent = Symbol("agent_$i")
    r = rv[1]
    for (j, r) in enumerate(rv)
        starttime_s = @sprintf("2020-03-%02iT%02i:00:00", sample(1:30), sample(0:24))
        starttime = DateTime(starttime_s)
        runvec[(i - 1)*4 + j] = Run((r, ), starttime, agent)
    end
end
runvec = RunVector(runvec)
# ============ Make model objects
# ------ Participation
prtprm = PrtcpModel(:(b0*1), Dict(:b0 => Inf))
# ------ Run state
run_f = quote
    if running == 1.0
        logit = b0_r + b1*ssrd + b2*dssrd
    else
        logit = b0_h + b1*ssrd + b2*dssrd
    end
end
run_β = Dict(:b0_r => 0., :b0_h => 0., :b1 => 0., :b2 => 0.)
runprm = RunModel(run_f, run_β)
# ------ Run velocity
vel_f = quote
    if running == 1.0
        return max(c*discharge, 0)
    else
        return 0
    end
end
velprm = VelocModel(vel_f, Dict(:c => 0.0005))
# ------ Init simulation object
time_step = 60.
max_iter = Int64(floor(60*60*24*5/time_step))
sim = Simulation(runvec, covs_dict,
                 runprm, prtprm, velprm,
                 time_step, meta = nothing)
# ============ Setup ABC functions
# --- Distance function
function f_dist(sim::Simulation, x_obs::Vector{Float64})
    # Get run time in seconds
    x_sim = (x -> (x.state.time - x.timestart).value/1000).(sim.runvec)
    return norm(x_sim .- x_obs)
end
# --- Function for updating model parameters
f_setβ = function(sim, β)
    setβ(sim; run = β)
end

# ========== Generate simulated dataset
f_setβ(sim, (b0_h = 3., b0_r = -5., b1 = -2., b2 = 0.))
reset(sim)
track = run(sim, max_iter, track = true)
sim.runvec
track.positions
track.running
track.reach_covariates
track.reach_covariates

plot(track, :ssrd)

plot(track, 2, :ssrd)
plot(track, 6, :ssrd)
plot(track, 5, :ssrd)

sim.prtprm
sim.runprm
sim.velprm
