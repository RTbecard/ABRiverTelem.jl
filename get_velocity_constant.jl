# get discharge constants for velocity equations
# Travel velocity will be: discharge*c
# c will be calculated by assuming at least 1 individual is constantly
# running through an entire reach.

using Dates
using Statistics
using ABRiverTelem
using PrettyTables
using DataFrames

function getvelconstant(covs_dict, runstats)

    # ------ Get a list of all reaches in dataset
    idx = findall(isequal.((x -> length(x.reaches)).(runstats), 1))
    rchs = unique((x -> x.reaches).(runstats[idx]))
    # ------ Init empty dictorary for saving constants of each reach
    constants = Dict{Symbol, Number}()
    summary = DataFrame(Reach = Vector{Symbol}(),
                        min_c = Vector{Float32}(),
                        n = Vector{Int32}(),
                        dist_km = Vector{Float32}(),
                        min_hours = Vector{Float32}(),
                        max_hours = Vector{Float32}(),
                        vel_000 = Vector{Float32}(),
                        vel_025 = Vector{Float32}(),
                        vel_050 = Vector{Float32}(),
                        vel_075 = Vector{Float32}(),
                        vel_100 = Vector{Float32}())
    for key in rchs
        reach = key[1] # remove surrountding tuple
        print(string("====== ", reach, " ======\n"))
        # skip if no discharge
        if :discharge ∉ keys(covs_dict[reach].reachcovs)
            continue
        end
        # ------ Get agents with matching reach tuples
        idx2 = findall((x -> x.reaches == key).(runstats))
        c = 0 # init constant for this reach
        velocities = Vector{Float32}()
        # Get reach length
        reach_len = covs_dict[reach].length
        # ------ Loop runstats for this reach
        for rs in runstats[idx2]
            print(rs)
            # Get start and end time of runstat
            timeidxtuple = (rs.timestart, rs.timestart + Second(rs.duration))
            # Get agent velocity
            run_duration = Dates.value(Second(timeidxtuple[2] - timeidxtuple[1]))
            vel_agent = reach_len / run_duration
            # get mean discharge for this run
            array = covs_dict[reach].reachcovs[:discharge]
            @debug """
            array: $(size(array.data))
            array start: $(array.timestart)
            array timestep: $(array.timedelta)
            timeidxtuple: $timeidxtuple
            """
            timevec = get(array, timeidxtuple, nothing)
            # remove missing
            timevec = filter(x -> !ismissing(x), timevec)
            # Save constant if higher value than existing value
            length(timevec) > 0 && (c = max(vel_agent/mean(timevec), c))
            # Save agent velocity for summary stats
            push!(velocities, vel_agent)
        end
        push!(summary, (reach, c*100, length(velocities), reach_len/1000,
                        reach_len/(maximum(velocities)*60*60),
                        reach_len/(minimum(velocities)*60*60),
                        quantile(velocities, (0, 0.25, 0.5, 0.75, 1))...))
        # save constant for this reach in results dict
        length(idx2) > 0 && (constants[reach] = c)
    end

    return summary
end
