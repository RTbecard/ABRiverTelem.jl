using Revise
using ABRiverTelem
using Debugger
using DataFrames
using Serialization
import GZip
using Dates
using Distributions
import Logging
# Enable debugging messages
ENV["JULIA_DEBUG"] = ABRiverTelem
ENV["JULIA_DEBUG"] = Main
Logging.Level(0) = Logging.Debug
# Set example file
xml_file = "~/Repos/ribes-esr02_datacuration/data/CCV-JSATS/covariates_2020_final.xml"
#using PyCall
# python debugging messages
#py"""
#import logging as log
#logger = log.getLogger()
#logger.setLevel(log.INFO)
#"""
#
# ========================= Load covariate data ===============================
covs_dict = readcovs(xml_file, 100.0)
runstats = readdetections(xml_file)
meta = readmeta(xml_file, missingstring = "NA")
# serialize and compress data so it can be quickly loaded later
gf = GZip.open("covariateData_2020_raw.jdata", "w")
serialize(gf, (covs_dict, runstats, meta))
close(gf)

# ===================== Load data from serialized object ======================
# i.e. previously saved raw data
gf = GZip.open("covariateData_2020_raw.jdata", "r")
(covs_dict, runstats, meta) = deserialize(gf)
close(gf)

# ============================== Filter Data ==================================
# list of reaches to remove from dataset
rm_reaches = []
# Mark reaches with no discharge data
for reach in reaches(covs_dict)
    [:discharge :FNU] ⊈ keys(covs_dict[reach]) && push!(rm_reaches ,reach)
end
# Filter out runs which require more that 5 days
runstats_filt = filter((x -> x.duration < 60*60*24*5), runstats)
# filter out runs in excluded reaches
filter!(x -> length(intersect(x.reaches, rm_reaches)) == 0, runstats_filt)
# Remove individuals with potentially missing covariate data
function chkcovs(rns, vars)
    # Check that all covariates are within run period
    for var in vars
        for rch in rns.reaches
            @debug "$rns $var $rch"
            arr = covs_dict[rch][var]
            time_end = arr.timestart + Second(size(arr.data)[1]*arr.timedelta)
            if rns.timestart + Day(5) > time_end
                return false
            end
        end
    end
    return true
end
filter(x -> chkcovs(x, [:discharge :FNU :ssrd]), runstats_filt)

# =================== Get velocity constant for each reach ====================
include("get_velocity_constant.jl")
velconst = getvelconstant(covs_dict, runstats_filt)

covs_dict[Symbol("113875979_2020_509")][:discharge]
# ========================== Simulation setup =================================

# ------ Run ob
run_f = quote
    logit = b0 + b1*discharge + b2*ssrd + b3*FNU + z[agent]
    if running
        logit = logit + log(b4)
    else
        logit = logit - log(b4)
    end
end
z_keys = ABRiverTelem.values(metafilt, :agent)
z_vals = rand(Normal(0,1), length(z_keys))
z = Dict{Symbol, AbstractFloat}(z_keys .=> z_vals)
betas = Dict(:b0 => 0., :b1 => 1., :z => z, :b2 => 2., :b3 => 2.)
runprm = RunModel(run_f, betas)
prtprm = PrtcpModel(:(b0*1), Dict(:b0 => Inf))
velprm = VelocModel(:(b0*1), Dict(:b0 => 0.1))
sim = Simulation(runvec_filt, covs_dict, runprm, prtprm, velprm, 60*10.0, meta = metafilt)

# maximum runtime of 5 days
run(sim, 720)
sim.runvec[1]

step(sim)
sim.runvec[1].state

reset(sim)

res_sim = results(sim)
res_obs = results(runstats)

# error here!
getlength(covs_dict, Symbol("113875979_2012_16"))

covs_dict[Symbol("129659503_2012_9")]
